var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');



router.get("/all", function (req, res) {
    resume_dal.getAll(function(err, resumes) {
        if (err) {
            res.send(err);
        }
        else {
            res.render("resume/resumeViewAll.ejs", {'result': resumes});
        }
    });
});

router.get("/add/selectuser", function(req, res) {
    account_dal.getAll(function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render("selectUser.ejs", {'result': result});
        }
    });
});

// TODO: procedure?
router.get("/add", function(req, res) {
    account_dal.getById(req.query.account_id, function(err, account) {
        if (err) {
            res.send(err);
        }
        else {
            school_dal.getByAccount(req.query.account_id, function(err, school) {
                if (err) {
                    res.send(err);
                }
                else {
                    company_dal.getByAccount(req.query.account_id, function (err, company) {
                        if (err) {
                            res.send(err);
                        }
                        else {
                            skill_dal.getByAccount(req.query.account_id, function (err, skill) {
                                if (err) {
                                    res.send(err);
                                }
                                else {
                                    res.render('resume/resumeAdd.ejs', {'account': req.query.account_id, 'school': school, 'company': company, 'skill': skill});
                                }
                            });
                        }
                    });
                }
            });
        }
    })
});

router.post("/insert", function (req, res) {
    resume_dal.insert(req.body, function(err, resume_id) {
        if (err) {
            console.log(err)
            res.send(err);
        }
        else {
            resume_dal.edit(resume_id, function(err, result) {
                res.render('resume/resumeUpdate', { resume_id: result[0][0]['resume_id'],
                    resume_name: result[0][0]['resume_name'],
                    school: result[1],
                    company: result[2],
                    skill: result[3],
                    selected_school: result[4],
                    selected_company: result[5],
                    selected_skill: result[6]
                });
            });
        }
    });
});

router.post("/update", function (req, res) {
    resume_dal.update(req.body, function(err, resume_id) {
        if (err) {
            console.log(err)
            res.send(err);
        }
        else {
            resume_dal.edit(resume_id, function(err, result) {
                res.render('resume/resumeUpdate', { resume_id: result[0][0]['resume_id'],
                    resume_name: result[0][0]['resume_name'],
                    school: result[1],
                    company: result[2],
                    skill: result[3],
                    selected_school: result[4],
                    selected_company: result[5],
                    selected_skill: result[6],
                    saved: true
                });
            });
        }
    });
});

router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                //res.redirect(302, '/resume/all');
            }
        });
    }
});



router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result){
            console.log(result[4]);
            console.log(result[4].map(function(a) {return a['school_id']}));
            res.render('resume/resumeUpdate', { resume_id: result[0][0]['resume_id'],
                resume_name: result[0][0]['resume_name'],
                school: result[1],
                company: result[2],
                skill: result[3],
                selected_school: result[4].map(function(a) {return a['school_id']}),
                selected_company: result[5].map(function(a) {return a['company_id']}),
                selected_skill: result[6].map(function(a) {return a['skill_id']})
            });
        });
    }

});


module.exports = router;

