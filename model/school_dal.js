var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM school;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(school_id, callback) {
    var query = 'SELECT * FROM school s ' +
        'WHERE s.school_id = ?';
    var queryData = [school_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getByAccount = function(account_id, callback) {
    var query = 'SELECT * FROM school s ' +
        'JOIN account_school acs on acs.school_id = s.school_id ' +
        'WHERE acs.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


/*exports.insert = function(params, callback) {
    var query = 'INSERT INTO school (first_name, last_name, email) VALUES (?, ?, ?)';

    var queryData = [params.first_name, params.last_name, params.email];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};*/

exports.delete = function(school_id, callback) {
    var query = 'DELETE FROM school WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};



/*exports.update = function(params, callback) {
    var query = 'UPDATE school SET first_name = ?, last_name = ?, email = ? WHERE school_id = ?';
    var queryData = [params.first_name, params.last_name, params.email, params.school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
*/