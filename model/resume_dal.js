var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM resume r ' +
        'JOIN account a on a.account_id = r.account_id ' +
        'ORDER BY a.first_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'SELECT c.*, a.* FROM resume r ' +
        'JOIN account a on a.account_id = r.account_id' +
        'WHERE r.resume_id = ?';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?, ?)';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, queryData, function(err, result) {
        console.log(err);
        var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
        var resume_id = result.insertId;

        var resume_school = [];
        for(var i = 0; i < params.school_id.length; i++) {
            resume_school.push([resume_id, params.school_id[i]]);
        }

        connection.query(query, [resume_school], function(err, result) {
            var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

            var resume_company = [];
            for(var i = 0; i < params.company_id.length; i++) {
                resume_company.push([resume_id, params.company_id[i]]);
            }

            connection.query(query, [resume_company], function(err, result) {
                var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

                var resume_skill = [];
                for(var i = 0; i < params.skill_id.length; i++) {
                    resume_skill.push([resume_id, params.skill_id[i]]);
                }

                connection.query(query, [resume_skill], function(err, result) {
                    callback(err, resume_id);
                });
            });
        });


    });
};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {

    connection.query("update resume set resume_name = ? where resume_id = ?", [params.resume_name, params.resume_id], function(err, result) {
    });
    connection.query("delete from resume_school where resume_id = ?", [params.resume_id], function(err, result){});
    connection.query("delete from resume_company where resume_id = ?", [params.resume_id], function(err, result){});
    connection.query("delete from resume_skill where resume_id = ?", [params.resume_id], function(err, result){});

    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
    var resume_id = params.resume_id;

    var resume_school = [];
    for(var i = 0; i < params.school_id.length; i++) {
        resume_school.push([resume_id, params.school_id[i]]);
    }

    connection.query(query, [resume_school], function(err, result) {
        var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

        var resume_company = [];
        for(var i = 0; i < params.company_id.length; i++) {
            resume_company.push([resume_id, params.company_id[i]]);
        }

        connection.query(query, [resume_company], function(err, result) {
            var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

            var resume_skill = [];
            for(var i = 0; i < params.skill_id.length; i++) {
                resume_skill.push([resume_id, params.skill_id[i]]);
            }

            connection.query(query, [resume_skill], function(err, result) {

            });
        });
    });

    callback(null, resume_id);

};



exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};